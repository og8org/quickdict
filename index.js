#!/usr/bin/env nodejs

const fs = require('fs')
const express = require('express')
const path = require('path')
const nunjucks = require('nunjucks')

// load search function
const search = require('./public/main.js').search

const app = express()
const port = 3000

const lang_names = ["eng_deu", "fra_deu", "spa_deu", "deu_ita", "ara_eng", "tur_deu", "deu_pol", "deu_ned"]

var lang_sizes = lang_names.map(function (lang) { return { name: lang, size: (fs.statSync("public/dict/"+lang+".gz")["size"]/1000.0/1024.0).toFixed(1) }})

// load local dict
dict = {}
lang_names.forEach(function (lang) {
  fs.readFile(lang+'_dict', (e,data) => dict[lang] = JSON.parse(data))
})

app.use(express.static(path.join(__dirname, 'public'), {'maxAge': '1y'}));

nunjucks.configure('views', {
    autoescape: true,
    express: app
});

app.get('/', (req, res) => res.render('index.njk', {languages: lang_sizes}))
app.get('/search', function (req, res) {
  query = req.query.query
  lang = req.query.lang
  console.log("SEARCH API : query: " + query)
  console.log("SEARCH API : lang: " + lang)
  if (query && query.length > 2 && lang && lang_names.indexOf(lang) != -1) {
    console.log("Params okay.")
    res.json( search(dict, query, lang))
  }
  else {
    console.log("Wrong params.")
    res.json({err: "e"})
  }
})

app.listen(port)
