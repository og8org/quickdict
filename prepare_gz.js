// zipping the dictionary

const fs = require('fs')
const zlib = require('zlib')
const gzip = zlib.createGzip()


fname = process.argv[2]
console.log("Processing : " + fname)

outfile = fname.replace("_dict", ".gz")

inp = fs.createReadStream(fname)
out = fs.createWriteStream(outfile)

inp.pipe(gzip).pipe(out)

console.log("Done.")
