#!/bin/python3

# TODO write in JS with parameter input

import json
import sys


try:
    fname = sys.argv[1]
except IndexError as e:
    print(" +++ First argument should be a valid dictionary with txt ending. ")
    print(" +++ ")
    raise(e)

outfile = fname.replace(".txt","_dict")

ff = open(fname,"r")
out = open(outfile,"w")
i = 0
arr = []
for line in ff:
  i += 1
  x = line.strip().split("\t")
  if len(x) > 2:
    arr.append({"o":x[0],"t":x[1], "typ":x[2]})
  else:
    arr.append({"o":x[0],"t":x[1], "typ":""})
out.write(json.dumps(arr))
