var dict = {}
var menu_toggle = false
var lang_toggle = false
var max_show = 25
var max_results = 250
var languages = ["eng_deu", "fra_deu", "spa_deu"]
var my_lang = "eng_deu"
var my_words = []
var api

function init() {
  document.domain = 'og8.org'
  // show right little flag
  document.getElementById("selected_language").style.backgroundImage = "url(img/"+my_lang+".png)"
  // connect to parent in iframe
  try {
    api = parent.api
    console.log(api ? "Working in iframe, talking to parent" : "Single mode. Living my own life.")
  } catch(e) {
    console.log(api ? "Working in iframe, talking to parent" : "Single mode. Living my own life.")
  }
}

function load_dict(lang) {
  show_msg("Loading ...")
  document.getElementById(lang+"_dl").style.display = "none"
  dl_dict(lang).then(result => unzip(result)).then(result => show_msg("Dictionary " + lang + " loaded and active.")).then(result => switch_language(lang))
}

async function dl_dict(lang) {
  let hwrap = document.getElementById(lang + "_circle")
  let hcnum = hwrap.getElementsByClassName("number_circle")[0]
  let hccolor = hwrap.getElementsByClassName("color_circle")[0]
  let hload = hwrap.getElementsByClassName("load_circle")[0]
  let perc = 0;
 
  hwrap.style.display = "inline-block"

  hload.style.animationPlayState = "running"

  const response = await fetch('dict/' + lang + '.gz')
  const reader = response.body.getReader()
 
  const contentLength = +response.headers.get('Content-Length')
  let receivedLength = 0
  let chunks = []
  let stepsize = contentLength/30.0
  let nextbar = stepsize
  while(true) {
   const chunk = await reader.read()

   if (chunk.done) {
    set_circle(hcnum, hccolor, 100)
    done_circle(hcnum, hccolor, hload)
    break;
   }

   chunks.push(chunk.value)
   receivedLength += chunk.value.length
   if (receivedLength > nextbar) {
     perc = Math.floor(receivedLength/contentLength*100.0)
     set_circle(hcnum, hccolor, perc)
     nextbar = receivedLength + stepsize
   }
  }
 
  let chunksMerged = new Uint8Array(receivedLength)
  let length = 0
  for(let chunk of chunks) {
   chunksMerged.set(chunk, length)
   length += chunk.length
  }
  return chunksMerged
}

function unzip(gz) {
  // create dict structur {"o": "apple", "t": "apfel", "typ": ""}
  const timeout = 3
  const chunksize = 4096*8
  const chunks = Math.floor(gz.length / chunksize)
  let inflate = new pako.Inflate()

  function wait(n) {
    setTimeout(function () {
      if (n<chunks) {
        chunk = gz.slice(n*chunksize,(n+1)*chunksize)
        inflate.push(chunk,false)
        //perc = Math.floor(n*100.0/chunks)
        //set_circle(hcnum, hccolor, perc)
        wait(n+1)
      }
      else {
        inflate.push(gz.slice(chunks*chunksize,(chunks+1)*chunksize), true)
        let dictstr = inflate.result
        dict[my_lang] = JSON.parse(new TextDecoder("utf8").decode(dictstr))
        //done_circle(hcnum, hccolor, hload)
      }
    }, timeout)
  }
  wait(0)
}

function set_circle(hcnum, hccolor, perc) {
  hcnum.innerHTML = perc
  hccolor.style.background = "linear-gradient(to top, #4CBF50 0%,#4CBF50 " + perc +"%,#f6f6f6 " + perc +"%,#f6f6f6 100%)"
}

function done_circle(hcnum, hccolor, hload) {
  set_circle(hcnum, hccolor, 100)
  hcnum.innerHTML = "\u2705"
  hload.style.animationPlayState = "paused"
  hload.style.border = "6px solid #3498db"
}

function trigger_search() {
  let query = document.getElementById("query").value
  query = query.trim()
  if (!dict.hasOwnProperty(my_lang)) // online search
    online_search(query).then(result => show_results(JSON.parse(result).results), error => console.log(error))
  else // locally
  {
    ret = search(dict, query, my_lang)
    if (ret.error)
      show_msg(ret.error)
    else
      show_results(ret.results)
  }
}

function search(dict, query, lang) {
  let results, constraint
  query = analyze_query(query)

  if (!query_valid(query)) {
    return {error: "Query " + query + " invalid. I need at least 3 characters."}
  }
  let filter_me = make_filter(query)
  let mark_pos_me = make_mark_pos(query)
  let score_me = make_score(query)

  results = dict[lang].filter(filter_me)
  if (results.length == 0) {
    return {error: "Nothing found.", results: [{o: "Nichts gefunden.", t: "Nothing found."}]}
  }
  try {
    results.forEach((dd) => score_me(dd))
    results = sort_me(results)
    results = results.slice(0,max_results)
    results = results.map(mark_pos_me)
    error = ""
  } catch(e) {
    error = e.message
  }
  return {error: error, results: results}
}

function make_filter(query) {
  let re  = new RegExp(query, "i")
  return (dd) => re.test(dd.o) || re.test(dd.t)
}

function make_mark_pos(query) {
  let re = new RegExp("("+query+")", 'i')
  //let re_par = new RegExp("\[([^\]]+)\]")
  let re_par = /\[([^\]]+)\]/g  // []
  let re_par2 = /\{([^\}]+)\}/g  // {}
  return function (dd) {
    let repO = dd.o.replace(re,'<em>$1</em>')
    let repT = dd.t.replace(re,'<em>$1</em>')
    repO = repO.replace(re_par,'<sub>$&</sub>')
    repT = repT.replace(re_par,'<sub>$&</sub>')
    repO = repO.replace(re_par2,'<sup>$&</sup>')
    repT = repT.replace(re_par2,'<sup>$&</sup>')
    return { o: repO, t: repT }
  }
}

function make_score(query) {
  let re  = new RegExp(query, "i")
  let cat_list = {verb: 1, noun: 2, adj: 3, adv: 4} // prefer nouns and verbs
  return function (dd) {
    let ltext = dd.o
    let ttext = dd.t
    let pos = dd.o.search(re)
    if (pos == -1) {
      ltext = dd.t
      ttext = dd.o
      pos = dd.t.search(re)
    }
    dd.score = [" ", undefined].includes(ltext[pos-1]) ? 0 : 500 // query is a new word
    dd.score += Object.keys(cat_list).includes(dd.typ) ? cat_list[dd.typ] : 30 // content of "good" type
    dd.score += hash_me(ltext) // group same words: grave, grave, grand
    dd.score += (pos+2)**2 // closer to beginning
    dd.score += (word_length(ltext) - query.length)**2 // shorter overall term
    dd.score += 0.1*(word_length(ttext) - query.length)**2 // shorter other language string
  }
}

function sort_me(results) {
  results.sort((a,b) => a.score - b.score)
  return results
}

function show_results(results, append) {
  let hbox = document.getElementById("outbox")
  let hres = document.getElementById("results")
  let fragment = document.createDocumentFragment()

  remove_dots(hbox)

  if (!append) hres.innerHTML = ""
  else {
    let div = document.createElement('div');
    div.className += "result-line"
    let divO = document.createElement('div');
    let divT = document.createElement('div');
    divO.className += "result-text"
    divT.className += "result-text"
    divO.innerHTML = " --- "
    divT.innerHTML = " --- "
    div.appendChild(divO)
    div.appendChild(divT)
    hres.appendChild(div)
    new_added = div
    div.scrollIntoView() // scroll to new elements
  }

  for (let n=0; n<Math.min(results.length,max_show); n++) {
    let div = document.createElement('div');
    div.className += "result-line"
    let divO = document.createElement('div');
    let divT = document.createElement('div');
    let div_icons = document.createElement('div');
    let div_img = document.createElement('div');
    divO.className += "result-text"
    divT.className += "result-text"
    div_icons.className += "result-icons"
    div_img.className += "result-img-add"
    div_img.onclick = () => add_word(results[n])
    divO.innerHTML = results[n].o
    divT.innerHTML = results[n].t
    div_icons.appendChild(div_img)
    div.appendChild(divO)
    div.appendChild(divT)
    div.appendChild(div_icons)
    fragment.appendChild(div)
  }
  hres.appendChild(fragment)
  if (results.length > max_show) append_dots(hbox,results)
}

function analyze_query(query) {
  if (!query.includes(" ")) return query // no blanks -> just search
  let articles = {3: {"der": "{m}", "die":"{f}", "das":"{n}", "les":"{pl}"}, 2: {"le": "{m}", "la": "{f}", "el": "{m}"}}
  let lower = query.toLowerCase()
  let ret
  for (const key in articles) {
    const art_len = key*1
    if (lower[art_len] === " ")
      if (Object.keys(articles[art_len]).includes(lower.slice(0,art_len))) {
        query = query.slice(art_len+1) + " " + articles[art_len][lower.slice(0,art_len)]
        break
      }
  }
  return query
}

function add_word(result) {
  if (api) { // all local
    api("add", result)
  }
  else {
    my_words.push(result)
    pr = new Promise(function (resolve, reject) {
      let xhr = new XMLHttpRequest()
      let url = 'https://turtles.og8.org/api/turtles/'
      xhr.open('POST', url)
      xhr.setRequestHeader('Accept', 'application/json')
      xhr.setRequestHeader('Content-Type', 'application/json')
      xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
          resolve(xhr.responseText)
        }
      }
      xhr.onerror = function () {
        reject({
          status: this.status,
          statusText: xhr.statusText,
          e: 391
        })
      }
      data = JSON.stringify({left: result.o, right: result.t, index:"Alle", difficulty: 1})
      console.log("Adding : " + data)
      xhr.send(data)
    })
    pr.then(result => console.log(result), error => console.log(error))
  }
}

function append_dots(hbox, results) {
  let div = document.createElement('div');
  div.innerHTML = "..."
  div.title = "Show more"
  div.className += "more_button"
  div.onclick = () => show_results(results.slice(max_show),append=true)
  hbox.appendChild(div)
}

function remove_dots(hbox) {
  let handle = hbox || document.getElementById("outbox")
  for (let element of handle.getElementsByClassName("more_button"))
    handle.removeChild(element)
}

function hash_me(s) {
  // create a small number from text
  let hash = 0, i, chr
  for (i = 0; i < s.length; i++) {
    chr   = s.charCodeAt(i)
    hash  = ((hash << 5) - hash) + chr
    hash |= 0; // convert to 32bit integer
  }
  return 1.0/hash
}

function word_length(word) {
  let shorter = word.split(" {")[0].split(" [")[0].length + 2 // parenthesis count as one
  return Math.min(shorter,word.length)
  //return word.length
}

function toggle_menu() {
  menu_toggle = !menu_toggle
  if (menu_toggle)
    document.getElementById("settings").style.display = "block"
  else
    document.getElementById("settings").style.display = "none"
}

function toggle_language() {
  lang_toggle = !lang_toggle
  let hh = document.getElementById("language_menu")
  if (lang_toggle)
    hh.style.display = "block"
  else
    hh.style.display = "none"
}

function close_language_menu() {
  lang_toggle = false
  document.getElementById("language_menu").style.display = "none"
}

function switch_language(lang) {
  if (lang != my_lang) {
    document.getElementById("selected_language").style.backgroundImage = "url(img/"+lang+".png)"
    my_lang = lang
  }
  close_language_menu()
}

function query_valid(s) {
  return s.length > 2
}

function show_msg(msg) {
  //document.getElementById("statusbar").innerHTML = msg
  console.log(msg)
  return
}

function online_search(query) {
  // sends request to server and calls search function of this exact file
  return new Promise(function (resolve, reject) {
    let xhr = new XMLHttpRequest()
    let url = '/search?query=' + query + '&lang=' + my_lang
    xhr.open('GET', url)
    xhr.onreadystatechange = function () {
      if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
        resolve(xhr.responseText)
      }
    }
    xhr.onerror = function () {
      reject({
        status: this.status,
        statusText: xhr.statusText,
        e: 5446
      })
    }
    xhr.send()
  })
}

// to run in node on server
try {
   module.exports = exports = { search: search };
} catch (e) {}
