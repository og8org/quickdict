# QuickDict

HTML5 dictionary to query words offline.
The dictionary can be downloaded so that the search is offline and super fast.
Many languages are supported with the dictionaries from dict.cc

A node server is needed to provide a list of the available languages.
It should include different parsers to handle word lists from different sources.

WIP

